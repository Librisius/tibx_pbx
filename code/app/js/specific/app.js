$(document).ready(function() {

	(function() {

		if ( $( '.c-select' ).length ) {

			$( '.c-select' ).each( function( index, el ) {

				var $this = $( this );

				// Be careful with the property dropdownParent!
				// When I added a container after .c-select, dropdown, acted weird.
				// Open one then click on another one, the previous one was closed,
				// but the current selectbox would be able to close only by clicking on itself,
				// and not outside

				$this.select2({
					theme: 'pbx',
					tags: true,
				}).on( 'select2:done', function( event ) {
					$( '.select2-results__options' ).addClass( 'c-scroll' ); // all styles bind to c-scroll class
					$( '.select2-results__options' ).perfectScrollbar({
						maxScrollbarLength: 100,
						minScrollbarLength: 60,
					});
				});
			});
		}
	})();

	(function() {

		if ( $( '.c-scroll' ).length ) {

			$( '.c-scroll' ).each( function( index, el ) {

				$( this ).perfectScrollbar({
					maxScrollbarLength: 115,
					minScrollbarLength: 60,
					wheelPropagation: true
				});

				if ( $( this ).hasClass('c-scroll_reach_end') ) {

					var className = $( this ).data( 'c-scroll-reach-end' );

					$( this ).on( 'ps-y-reach-end' , function() {

						$( this ).addClass( className );
					});

					$( this ).on( 'ps-scroll-up' , function() {

						if ( $( this ).hasClass( className ) ) {

							$( this ).removeClass( className );
						}
					});
				}
			});
		}
	})();

	(function() {

		if ( $('.c-tooltip').length ) {

			$('.c-tooltip').tooltipster({
				contentCloning: true,
				delay: 0,
				maxWidth: 300,
				theme: 'tooltipster_pbx',
			});
		}
	})();

	(function() {

		if ( $( '.c-switcher' ).length ) {

			$( '.c-switcher' ).each( function( index, el ) {

				var $this = $( this );

				$this.on( 'click', function( event ) {

					addingDataAttribute( $this );
				});

				function addingDataAttribute( $this ) {

					var $element	= ( $this.data( 'c-switcher-element' ) || $this ) ? ( $this.data( 'c-switcher-element' ).split(', ') || $this ) : undefined,
						className	= $this.data( 'c-switcher-class' ) ? $this.data( 'c-switcher-class' ).split(', ') : undefined,
						$closest	= $this.data( 'c-switcher-closest' ) ? $this.data( 'c-switcher-closest' ).split(', ') : undefined;

					$element.forEach( function( el, i ) {
						if ( className ? className[ i ] : false ) {
							if ( $closest ? $closest[ i ] : false ) {
								$( $element[ i ] ).closest( $closest[ i ] ).toggleClass( className[ i ] );
							} else {
								$( el ).toggleClass( className[ i ] );
							}
						}
					});
				};
			});
		}
	})();

	(function() {

		$( document ).on( 'keyup', function( evt ) {
			if ( evt.keyCode == 27 ) {

				if ( $( 'body').hasClass( '_modal_active' ) ) {
					$( '.b-modal_active .b-modal__close' ).click();
				}
			}
		});
	})();

	(function() {

		if ( $( '.c-manipulate-class' ).length ) {

			$( '.c-manipulate-class' ).each( function( index, el ) {

				$( this ).on( 'click', function( event ) {
					var $this			= $( this ),
						$element		= ( $this.data( 'c-manipulate-class-element' ) || $this ) ? ( $this.data( 'c-manipulate-class-element' ).split(', ') || $this ) : undefined,
						className		= $this.data( 'c-manipulate-class-class' ) ? $this.data( 'c-manipulate-class-class' ).split(', ') : undefined,
						operationType	= $this.data( 'c-manipulate-class-operation-type' ) ? $this.data( 'c-manipulate-class-operation-type' ).split(', ') : undefined,
						$closest		= $this.data( 'c-manipulate-class-closest' ) ? $this.data( 'c-manipulate-class-closest' ).split(', ') : undefined;

					$element.forEach( function( el, i ) {
						if ( className ? className[ i ] : false ) {
							if ( $closest ? $closest[ i ] : false ) {
								if ( operationType ? operationType[ i ] == 'remove' : false ) {
									$( $element[ i ] ).closest( $closest[ i ] ).removeClass( className[ i ] );
								} else {
									$( $element[ i ] ).closest( $closest[ i ] ).addClass( className[ i ] );
								}
							} else {
								if ( operationType ? operationType[ i ] == 'remove' : false ) {
									$( el ).removeClass( className[ i ] );
								} else {
									$( el ).addClass( className[ i ] );
								}
							}
						}
					});
				});
			});
		}
	})();
});

$(window).on( 'load', function() {

	(function() {

		if ( $('.c-block').length ) {
		}
	})();
});